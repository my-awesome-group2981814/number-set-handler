package org.example;

import java.io.*;
import java.util.Arrays;

public class App {
    private static File file = new File("C:\\Users\\dotka\\OneDrive\\Рабочий стол\\10m.txt");
    private static long[] numbers = new long[9_999_999];

    public static void main( String[] args ) throws IOException {
        fillArr();
        System.out.println(longestAscSequence());
        System.out.println(longestDescSequence());
        System.out.println(findAvg());
        Arrays.sort(numbers);
        System.out.println(findMedian());
        System.out.println(min());
        System.out.println(max());

    }

    private static void fillArr() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Long.parseLong(reader.readLine());
        }
        reader.close();
    }

    private static long max() {
        return numbers[numbers.length - 1];
    }

    private static long min() {
        return numbers[0];
    }

    private static long findMedian() {
        if (numbers.length % 2 == 0) return (numbers[numbers.length / 2] + numbers[numbers.length / 2 - 1]) / 2;
        return numbers[numbers.length / 2];
    }

    private static long findAvg() {
        long sum = 0;
        for (long l : numbers) sum += l;
        return sum / numbers.length;
    }

    private static int longestAscSequence() {
        int maxLength = 1;
        int currentLength = 1;
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] > numbers[i - 1]) {
                currentLength++;
            } else {
                if (currentLength > maxLength) maxLength = currentLength;
                currentLength = 1;
            }
        }
        return maxLength;
    }

    private static int longestDescSequence() {
        int maxLength = 1;
        int currentLength = 1;
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] < numbers[i - 1]) {
                currentLength++;
            } else {
                if (currentLength > maxLength) maxLength = currentLength;
                currentLength = 1;
            }
        }
        return maxLength;
    }
}
